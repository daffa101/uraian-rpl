import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Container> daftarPilihan = new List();

  // Array nama, keterangan, gambar
  var pilihan = [
    {
      "nama": "Tentang RPL",
      "gambar": "rpl.png",
      "deskripsi":
          "Program Keahlian Rekayasa Perangkat Lunak (RPL) didirikan sejak tahun 2003 Rekayasa Perangkat Lunak adalah salah satu kompetensi keahlian dalam bidang Teknologi Komputer dan Informatika yang secara khusus mempelajari tentang pemrograman komputer. Lulusan RPL sangat dibutuhkan untuk menjawab banyaknya kebutuhan industri bagi ketersediaan tenaga Teknisi dalam bidang Rekayasa Perangkat Lunak."
    },
    {
      "nama": "Struktur",
      "gambar": "str2.png",
      "deskripsi":
          "Ketua Kompetensi: Yudi Subekti, S.Kom, //// Guru: Ani Nuraeni, S.Kom, Drs. Asep Eka Setia Priatna, M.Si. Himatul Munawaroh. ST, Rini Melati. S.Kom."
    },
    {
      "nama": "Visi Misi",
      "gambar": "visimisi.png",
      "deskripsi":
          "VISI : “Mencetak lulusan yang unggul, berbudaya dan berwawasan lingkungan serta memiliki komptensi di bidang teknologi informasi dan komunikasi khususnya rekayasa perangkat lunak “ // // // // // // // //MISI : •	Membekali peserta didik dengan pengetahuan, sikap dan keterampilan dalam bidang teknologi informasi agar kompeten dibidangnya •	Menghasilkan lulusan yang berkualitas dan mampu bersaing didunia kerja maupun industry di bidang teknologi informasi khususnya Rekayasa Perangkat Lunak •	Membekali siswa agar berprestasi dibidangnya baik ditingkat lokal, nasional maupun regional •	Membekali siswa agar mampu berwirausaha dibidangnya khususnya dan umumnya dibidang lain"
    },
    {
      "nama": "Peluang Kerja",
      "gambar": "pel_kerja.png",
      "deskripsi":
          "Dengan memanfaatkan kemampuan, kompetensi, pengalaman dan berbagai peluang yang ada, lulusan Program Keahlian Rekayasa Perangkat Lunak diharapkan akan bisa menjadi :	•	Web Application Programmer •	Database Programmer •	Interfacing Programmer • Mobile Application Programmer (Java and Android) •	Desktop Application Programmer •	C and C++ Programmer •	Game Programmer •	Hardware and Software Technicians •	IT Support and IT Staff •	Pekerjaan-pekerjaan lainnya yang berbasis komputer"
    },
    {
      "nama": "Kurikulum",
      "gambar": "kur.png",
      "deskripsi":
          "Fisika, Pemograman Dasar, Sistem Komputer, Pemodelan Perangkat Lunak, Pemograman Desktop, Pemograman Beroientasi Objek, Basis Data, DLL"
    },
    {
      "nama": "Prestasi RPL",
      "gambar": "award.png",
      "deskripsi": "Sigit dan Adit : Juara 3 Design Wen"
    },
  ];

  // function grid menu pilihan
  _buatList() async {
    for (var i = 0; i < pilihan.length; i++) {
      final pilihannya = pilihan[i];
      final String gambar = pilihannya["gambar"];

      daftarPilihan.add(new Container(
        padding: new EdgeInsets.all(3.0),
        child: new Card(
            child: new Column(
          children: <Widget>[
            new Hero(
              tag: pilihannya["nama"],
              child: new Material(
                child: new InkWell(
                  onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new Detail(
                            nama: pilihannya["nama"],
                            gambar: gambar,
                            deskripsi: pilihannya["deskripsi"],
                          ))),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: new Image.asset(
                      "assets/images/$gambar",
                      width: 100.0,
                      height: 100.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7.0),
            ),
            new Text(
              pilihannya["nama"],
              style: new TextStyle(fontSize: 18.0),
            )
          ],
        )),
      ));
    }
  }

  @override
  void initState() {
    _buatList();
    super.initState();
  }

  // Appbar
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Rekayasa Perangkat Lunak"),
      ),
      body: new GridView.count(
        crossAxisCount: 2,
        children: daftarPilihan,
      ),
    );
  }
}

// Page detail dari pilihan
class Detail extends StatelessWidget {
  Detail({this.nama, this.gambar, this.deskripsi});
  final String nama;
  final String gambar;
  final String deskripsi;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new ListView(
        children: <Widget>[
          new Container(
              height: 240.0,
              child: new Hero(
                tag: nama,
                child: new Material(
                  child: new InkWell(
                    child: new Image.asset("assets/images/$gambar"),
                  ),
                ),
              )),
          new BagianNama(
            nama: nama,
          ),
          new BagianIcon(),
          new BagianKeterangan(
            deskripsi: deskripsi,
          ),
        ],
      ),
    );
  }
}

class BagianNama extends StatelessWidget {
  BagianNama({this.nama});
  final String nama;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  nama,
                  style: new TextStyle(fontSize: 19.0, color: Colors.green),
                ),
                new Text(
                  "smkn11bdg\@gmail.com",
                  style:
                      new TextStyle(fontSize: 17.0, color: Colors.greenAccent),
                ),
              ],
            ),
          ),
          new Row(
            children: <Widget>[
              new Icon(
                Icons.star,
                size: 20.0,
                color: Colors.blue,
              ),
              new Text(
                "10",
                style: new TextStyle(fontSize: 12.0),
              )
            ],
          )
        ],
      ),
    );
  }
}

class BagianIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(7.0),
      child: new Row(
        children: <Widget>[
          new Iconteks(
            icon: Icons.call,
            teks: "Call",
          ),
          new Iconteks(
            icon: Icons.message,
            teks: "Message",
          ),
          new Iconteks(
            icon: Icons.photo,
            teks: "Photo",
          ),
        ],
      ),
    );
  }
}

class Iconteks extends StatelessWidget {
  Iconteks({this.icon, this.teks});
  final IconData icon;
  final String teks;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: new Column(
        children: <Widget>[
          new Icon(
            icon,
            size: 40.0,
            color: Colors.green,
          ),
          new Text(teks,
              style: new TextStyle(fontSize: 15.0, color: Colors.green))
        ],
      ),
    );
  }
}

class BagianKeterangan extends StatelessWidget {
  BagianKeterangan({this.deskripsi});
  final String deskripsi;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(7.0),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: new Text(
          deskripsi,
          style: new TextStyle(fontSize: 15.0),
          textAlign: TextAlign.justify,
        ),
      ),
    );
  }
}
