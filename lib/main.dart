import 'package:flutter/material.dart';
import 'package:rpl/splashscreen_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Struktur Organisasi RPL',
      theme: ThemeData(
          fontFamily: "NeoSans",
          primaryColor: Colors.green,
          accentColor: Colors.lightGreen),
      home: SplashScreen(),
    );
  }
}
